import os
from setuptools import setup, find_packages

project_dir = os.path.dirname(os.path.realpath(__file__))
requirement_file_path = project_dir + '/../requirements.txt'
requirements = []
if os.path.isfile(requirement_file_path):
	with open(requirement_file_path) as f:
		requirements = f.read().splitlines()

setup(
	name='mey-snake-basket',
	version='0.0.1-dev1',
	author='Laura MEYER',
	author_email='laura.meyer@telecomnancy.net',
	url='https://bitbucket.org/lauraalexandrameyer/snake-basket',
	description='A sample project.',
	long_description=open('README.rst').read(),

	packages=find_packages(exclude=['tests', 'data']),
	include_package_data=True,
	install_requires=requirements,

	classifiers=[
		'Programming Language :: Python',
		'Development Status :: 1 - Planning',
		'Operating system :: OS independent',
		'Programming Language :: Python :: 3.6',
		'Intended Audience :: Education'
	]
)
